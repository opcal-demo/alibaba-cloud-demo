package opcal.cloud.demo.dnd.calculator.client.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;
import opcal.cloud.demo.dnd.calculator.api.service.CalculateService;

@Slf4j
@RestController
@RequestMapping("/calculate")
public class DndCalculatorController {

	@DubboReference
	private CalculateService calculateService;

	@PostMapping("/calculate1")
	@HystrixCommand(fallbackMethod = "calculate1Fallback")
	public double calculate1(@RequestParam double x, @RequestParam double y) {
		return calculateService.calculate1(x, y);
	}

	double calculate1Fallback(double x, double y) {
		log.info("calculate1 fallback");
		return Double.NaN;
	}

	@PostMapping("/calculate2")
	@HystrixCommand(fallbackMethod = "calculate2Fallback")
	public double calculate2(@RequestParam double x, @RequestParam double y, @RequestParam double z) {
		return calculateService.calculate2(x, y, z);
	}

	double calculate2Fallback(double x, double y, double z) {
		log.info("calculate2 fallback");
		return Double.NaN;
	}
}
