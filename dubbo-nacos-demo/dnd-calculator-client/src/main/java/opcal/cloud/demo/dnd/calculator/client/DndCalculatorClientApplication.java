package opcal.cloud.demo.dnd.calculator.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@EnableAutoConfiguration
@SpringBootApplication
@EnableHystrix
public class DndCalculatorClientApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(DndCalculatorClientApplication.class, args);
	}

}
