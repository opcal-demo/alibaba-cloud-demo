# dubbo-nacos-demo
This is a serial spring cloud dubbo nacos demos.

## dnd-calculator-server

```sh
 ./mvnw install -DskipTests=true && ./mvnw spring-boot:run -pl ./dubbo-nacos-demo/dnd-calculator-server
```

## dnd-calculator-client

```sh
 ./mvnw install -DskipTests=true && ./mvnw spring-boot:run -pl ./dubbo-nacos-demo/dnd-calculator-client
```