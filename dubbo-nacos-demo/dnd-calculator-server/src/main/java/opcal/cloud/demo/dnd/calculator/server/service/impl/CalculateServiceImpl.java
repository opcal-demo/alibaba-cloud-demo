package opcal.cloud.demo.dnd.calculator.server.service.impl;

import java.util.Random;

import org.apache.dubbo.config.annotation.DubboService;

import opcal.cloud.demo.dnd.calculator.api.service.CalculateService;

@DubboService
public class CalculateServiceImpl implements CalculateService {

	@Override
	public double calculate1(double x, double y) {
		Random xfactor = new Random(Double.valueOf(x).longValue());
		return Math.pow(x, xfactor.nextInt(10)) * Math.sin(y);
	}

	@Override
	public double calculate2(double x, double y, double z) {
		return Math.sin(x) + Math.cos(y) * Math.tan(z);
	}

}
