package opcal.cloud.demo.dnd.calculator.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@EnableAutoConfiguration
@SpringBootApplication
public class DndCalculatorServerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(DndCalculatorServerApplication.class, args);
	}

}
