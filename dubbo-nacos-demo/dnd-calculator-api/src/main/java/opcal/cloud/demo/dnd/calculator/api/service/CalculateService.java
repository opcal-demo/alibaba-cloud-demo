package opcal.cloud.demo.dnd.calculator.api.service;

public interface CalculateService {

	double calculate1(double x, double y);

	double calculate2(double x, double y, double z);
}
