package opcal.cloud.demo.nd.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class NdZuulApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(NdZuulApplication.class, args);
	}

}