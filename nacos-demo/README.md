# nacos-demo

This is a serial of demos using nacos as service registration.

## nd-config-server

run from root

```sh
./mvnw spring-boot:run -pl ./nacos-demo/nd-config-server
```

run from current

```sh
../mvnw spring-boot:run -pl nd-config-server
```

## nd-gateway

run from root

```sh
./mvnw spring-boot:run -pl ./nacos-demo/nd-gateway
```

run from current

```sh
../mvnw spring-boot:run -pl nd-gateway
```

## nd-zuul

run from root

```sh
./mvnw spring-boot:run -pl ./nacos-demo/nd-zuul
```

run from current

```sh
../mvnw spring-boot:run -pl nd-zuul
```

## nd-service-demo

This is a serial of [mocking business demos](nd-service-demo/README.md).


## service with gateway api demo

### gateway

```sh
curl --location --request POST '127.0.0.1:10381/nd-a-s/mock-nd-a-native/nd-a-calculate?x=18.91&y=19.2109'
curl --location --request POST '127.0.0.1:10381/nd-a-s/mock-nd-a-cross/a-calculate-b2?x=810'
curl --location --request POST '127.0.0.1:10381/nd-a-s/mock-nd-a-cross/a-calculate-b3?x=2.8912&y=343.81'

curl --location --request POST '127.0.0.1:10381/nd-b-s/mock-nd-b-native/nd-b-calcul-2?x=0.1298&y=103.81'
curl --location --request POST '127.0.0.1:10381/nd-b-s/mock-nd-b-native/nd-b-calcul-3?x=232.8&y=36.9281&z=431'
curl --location --request POST '127.0.0.1:10381/nd-b-s/mock-nd-b-cross/b-calculate-a?x=0.76'
```

### zuul

```sh
curl --location --request POST '127.0.0.1:10382/nd-a-s/mock-nd-a-native/nd-a-calculate?x=18.91&y=19.2109'
curl --location --request POST '127.0.0.1:10382/nd-a-s/mock-nd-a-cross/a-calculate-b2?x=810'
curl --location --request POST '127.0.0.1:10382/nd-a-s/mock-nd-a-cross/a-calculate-b3?x=2.8912&y=343.81'

curl --location --request POST '127.0.0.1:10382/nd-b-s/mock-nd-b-native/nd-b-calcul-2?x=0.1298&y=103.81'
curl --location --request POST '127.0.0.1:10382/nd-b-s/mock-nd-b-native/nd-b-calcul-3?x=232.8&y=36.9281&z=431'
curl --location --request POST '127.0.0.1:10382/nd-b-s/mock-nd-b-cross/b-calculate-a?x=0.76'
```
