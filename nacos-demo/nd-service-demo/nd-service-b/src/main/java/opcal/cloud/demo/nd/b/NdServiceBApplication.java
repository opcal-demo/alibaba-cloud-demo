package opcal.cloud.demo.nd.b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class NdServiceBApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(NdServiceBApplication.class, args);
	}

}