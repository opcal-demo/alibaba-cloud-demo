package opcal.cloud.demo.nd.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-nd-b-native")
public class NdBOriginalController {

	@Value("${nd.b.calculate-factor:0.9}")
	private double factor;

	@PostMapping("/nd-b-calcul-2")
	public double ndBCalculate(@RequestParam Double x, @RequestParam Double y) {
		return Math.ceil(x) * Math.expm1(y) / factor;
	}

	@PostMapping("/nd-b-calcul-3")
	public double ndBCalculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z) {
		Random zfactor = new Random(z.longValue());
		return Math.tan(x) + Math.sin(y) - (zfactor.nextDouble() - z);
	}

}
