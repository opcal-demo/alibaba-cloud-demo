package opcal.cloud.demo.nd.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.nd.b.service.client.fallback.factory.NdAApiServiceFallbackFactory;

@FeignClient(name = "nd-service-a", path = "/mock-nd-a-native", fallbackFactory = NdAApiServiceFallbackFactory.class)
public interface NdAApiService {

	@PostMapping("/nd-a-calculate")
	double ndACalculate(@RequestParam Double x, @RequestParam Double y);
}
