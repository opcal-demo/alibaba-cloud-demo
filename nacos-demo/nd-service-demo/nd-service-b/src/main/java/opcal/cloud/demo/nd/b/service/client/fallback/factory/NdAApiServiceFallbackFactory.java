package opcal.cloud.demo.nd.b.service.client.fallback.factory;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import opcal.cloud.demo.nd.b.service.client.NdAApiService;

@Component
public class NdAApiServiceFallbackFactory implements FallbackFactory<NdAApiService> {

	@Override
	public NdAApiService create(Throwable cause) {
		return new NdAApiService() {

			@Override
			public double ndACalculate(Double x, Double y) {
				return Double.NaN;
			}
		};
	}

}
