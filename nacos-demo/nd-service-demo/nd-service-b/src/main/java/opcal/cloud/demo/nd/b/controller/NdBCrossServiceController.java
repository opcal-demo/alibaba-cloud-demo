package opcal.cloud.demo.nd.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.nd.b.service.client.NdAApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-nd-b-cross")
public class NdBCrossServiceController {

	private @Autowired NdAApiService ndAApiService;

	@PostMapping("/b-calculate-a")
	public double aCalculateB2(@RequestParam Double x) {
		Random xFactor = new Random(x.longValue());
		return ndAApiService.ndACalculate(x, xFactor.nextDouble());
	}

}
