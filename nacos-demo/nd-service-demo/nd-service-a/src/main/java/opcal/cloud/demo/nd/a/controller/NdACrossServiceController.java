package opcal.cloud.demo.nd.a.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.nd.a.service.client.NdBApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-nd-a-cross")
public class NdACrossServiceController {

	private @Autowired NdBApiService ndBApiService;

	@PostMapping("/a-calculate-b2")
	public double aCalculateB2(@RequestParam Long x) {
		Random xFactor = new Random(x);
		return ndBApiService.ndBCalculate(x.doubleValue(), xFactor.nextDouble());
	}

	@PostMapping("/a-calculate-b3")
	public double aCalculateB3(@RequestParam Double x, @RequestParam Double y) {
		Random yFactor = new Random(y.longValue());
		return ndBApiService.ndBCalculate(x, y, Math.sin(yFactor.nextDouble()));
	}
}
