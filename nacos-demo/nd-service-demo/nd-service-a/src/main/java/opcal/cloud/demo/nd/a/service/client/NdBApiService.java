package opcal.cloud.demo.nd.a.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.nd.a.service.client.fallback.NdBApiServiceHystrix;

@FeignClient(name = "nd-service-b", path = "/mock-nd-b-native", fallback = NdBApiServiceHystrix.class)
public interface NdBApiService {

	@PostMapping("/nd-b-calcul-2")
	double ndBCalculate(@RequestParam Double x, @RequestParam Double y);

	@PostMapping("/nd-b-calcul-3")
	double ndBCalculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z);
}
