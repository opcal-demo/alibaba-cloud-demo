package opcal.cloud.demo.nd.a.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-nd-a-native")
public class NdAOriginalController {

	@PostMapping("/nd-a-calculate")
	public double ndACalculate(@RequestParam Double x, @RequestParam Double y) {
		return Math.log(x) / Math.PI * Math.sin(y);
	}
}
