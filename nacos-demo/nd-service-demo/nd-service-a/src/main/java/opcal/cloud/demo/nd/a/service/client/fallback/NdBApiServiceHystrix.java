package opcal.cloud.demo.nd.a.service.client.fallback;

import org.springframework.stereotype.Component;

import opcal.cloud.demo.nd.a.service.client.NdBApiService;

@Component
public class NdBApiServiceHystrix implements NdBApiService {

	@Override
	public double ndBCalculate(Double x, Double y) {
		return Double.NaN;
	}

	@Override
	public double ndBCalculate(Double x, Double y, Double z) {
		return Double.NaN;
	}

}
