# nd-service-demo

## nd-service-a

run from root

```sh
./mvnw spring-boot:run -pl ./nacos-demo/nd-service-demo/nd-service-a
```

run from current

```sh
../../mvnw spring-boot:run -pl nd-service-a
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10383/mock-nd-a-native/nd-a-calculate?x=18.91&y=19.2109'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10383/mock-nd-a-cross/a-calculate-b2?x=810'
curl --location --request POST '127.0.0.1:10383/mock-nd-a-cross/a-calculate-b3?x=2.8912&y=343.81'
```


## nd-service-b

run from root

```sh
./mvnw spring-boot:run -pl ./nacos-demo/nd-service-demo/nd-service-b
```

run from current

```sh
../../mvnw spring-boot:run -pl nd-service-b
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10384/mock-nd-b-native/nd-b-calcul-2?x=0.1298&y=103.81'
curl --location --request POST '127.0.0.1:10384/mock-nd-b-native/nd-b-calcul-3?x=232.8&y=36.9281&z=431'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10384/mock-nd-b-cross/b-calculate-a?x=0.76'
```