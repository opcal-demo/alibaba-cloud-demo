# Alibaba-cloud-demo
This a serial of demos using Spring Cloud Alibaba.

Java Version 11

Spring Boot Version 2.3.4.RELEASE

Spring Cloud Version Hoxton.SR8

Spring Cloud Alibaba Version 2.2.3.RELEASE


## Nacos-demo

[Nacos Demo](nacos-demo/README.md) is a serial of demos using nacos service registration.

## Configuration design intention

The purpose of the design is to prototype another demo project [cloud-demo](https://gitlab.com/opcal-demo/cloud-demo#configuration), for more detailed description, please check the description of that project.

## A dubbo problem issue

Since Spring Cloud Alibaba Version 2.2.1.RELEASE, spring cloud dubbo has a common problem, detail in [issue #1360](https://github.com/alibaba/spring-cloud-alibaba/issues/1360).

Version 2.2.2.RELEASE the problem is still existing, detail in [issue #1732](https://github.com/alibaba/spring-cloud-alibaba/issues/1732).

Current Version 2.2.3.RELEASE the problem is still existing. So demo project [dubbo-nacos-demo/dnd-calculator-client] is failed to start up.